
@include('_layout.header')

<div class="row" style="margin-top:5%;">
<div class="col-12">
<table class="table">
<tr>
<th>Title</th>
<th>Body</th>
<th>FB Link</th>
<th>Actions</th>
<tr>
@foreach($post as $pst)
<tr>
<td>{{$pst->title}}</td>
<td>{{substr($pst->body,0,200)}}</td>
<td>{{$pst->fb_link}}</td>
<td>
<a href="{{asset('').'post/'.$pst->id}}"><button class="btn btn-primary">Show</button><a/>&nbsp&nbsp
<a href="{{asset('').'post/'.$pst->id.'/edit'}}"><button class="btn btn-info">Edit</button><a/>&nbsp
<form method="POST" style="display:inline" action="{{route('post.destroy',['id'=>$pst->id])}}">
@csrf
{{ method_field('DELETE') }}

<input class="btn btn-danger" type="submit" value="Delete" name="delete">
</form>
</td>
</tr>
@endforeach
</table>
</div>
</div>
@include('_layout.footer')
