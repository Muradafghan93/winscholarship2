
<div class="container-fluid">
@include('_layout.header')




<div class="row" style="padding:1%">
<div class="container">

<form class="form-group" method="POST" action="../post" >
@csrf

<label>Title</label>
<input type="text" class="form-control" name="title"><br>
<label>Body</label>
<textarea type="text" class="form-control" name="body"></textarea><br>
<label>Facebook Link</label>
<input type="text" class="form-control" name="fb_link"><br>


<div class="col-12">
        <div class="row">

                  <div>
                      <input class="hiddeninput"  hidden type="text" value="location" name="location_id">
                      <label>Location</label>
                      <select class="form-control">
                      <option class="form-control" value="null">null</option>
                        @foreach($lookups_loc as $lookup)
                        <option    value="{{$lookup->id}}">{{$lookup->name}}</option>
                        @endforeach
                      </select>

                  </div>
                  <div>
                        <input class="hiddeninput" hidden type="text" value="Field"  name="field_of_study_id">
                        <label>Field of Study</label>
                        <select class="form-control">
                        <option  class="form-control" value="null">null</option>
                          @foreach($lookups_field as $lookup)
                          <option  value="{{$lookup->id}}">{{$lookup->name}}</option>
                          @endforeach
                        </select>

                  </div>
                  <div>

                          <input hidden type="text" value="Level" name="study_level_id">
                          <label>Study Level ID</label>
                          <select class="form-control">
                          <option class="form-control" value="null">null</option>
                            @foreach($lookups_level as $lookup)
                            <option   value="{{$lookup->id}}">{{$lookup->name}}</option>
                            @endforeach
                          </select>

                  </div>
                  <div>

                        <input hidden value="English" type="text" name="english_requirment_id">
                        <label>English Requirment</label>
                        <select class="form-control">
                        <option class="form-control" value="null">null</option>
                          @foreach($lookups_req as $lookup)
                          <option   value="{{$lookup->id}}">{{$lookup->name}}</option>
                          @endforeach
                        </select>
                        <br>

                  </div>
                  <div>
                        <input hidden type="text" value="Abroad" name="abroad">
                        <label>Is Abroad</label>
                        <select class="form-control">
                        <option class="form-control" value="1">YES</option>
                        <option class="form-control" value="0">NO</option>
                        </select>
                  </div>
        </div>

</div>


<label>End Date</label>
<input type="date" class="form-control" name="end_date"><br>
<input type="file" class="form-control" name="post_photo"><br>
<br>
<input type="submit" >
</form>

</div>
</div>
</div>

<script>
$('select').change(function(){ $(this).closest('div').find('input').val($(this).val()); console.log($(this).closest('div').find('input').val());});
</script>
@include('_layout.footer')
</div>

