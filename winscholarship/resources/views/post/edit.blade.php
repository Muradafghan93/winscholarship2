
@include('_layout.header')
<div class="row">
<div class="col-12">
<div class="container">

<form class="form-group" method="POST" action="{{route('post.update',['id'=>$post->id])}}" >
@csrf
{{ method_field('PATCH') }}
<label>Post Title</label>

<input type="text" name="title" class="form-control" value="{{$post->title}}"><br>
<label>Body</label>
<textarea type="text" name="body" class="form-control" value="{{$post->body}}">{{$post->body}} </textarea> <br>
<label>Facebook Link</label>
<input type="text" name="fb_link" class="form-control"  value="{{$post->fb_link}}"><br>
<label>End Date</label>
<input type="date" name="end_date" class="form-control" value="{{$post->end_date}}"><br>
<input type="file"  class="form-control" name="post_photo"><br>
<br>
<input type="submit" >
</form>

</div>
</div>
</div>
@include('_layout.footer')
