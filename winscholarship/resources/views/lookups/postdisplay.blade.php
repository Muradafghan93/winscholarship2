
@include('_layout.header')

@foreach($posts as $post)

    <h1>{{$post->title}}</h1>
    <p>{{$post->body}}</p>

@endforeach
                        <ul class="pagination">
                          @for ($i = 1; $i <= $posts->lastPage(); $i++)
                            <li  @if($posts->currentPage() == $i) class="active" @endif><span>{{ $i }}</span></li>
                          @endfor
                        </ul>

@include('_layout.footer')

