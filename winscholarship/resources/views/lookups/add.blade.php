
@include('_layout.header')




<div class="row">
<div class="col-12">
<div class="container">

<form class="form-group" method="POST" action="../lookups" >
@csrf

<label>name</label>
<input type="text" class="form-control" name="name"><br>

<input hidden type="text" name="cat_id">
<label>Cat</label>
<select>
<option  value="null">null</option>

  @foreach($lookups_cat as $lookup)
  <option class="cat_id_option"  value="{{$lookup->id}}">{{$lookup->name}}</option>
  @endforeach
</select>

<input hidden type="text" name="parent_id">
<label>Parent</label>
<select >
<option  value="null">null</option>
  @foreach($lookups as $lookup)
  <option class="parent_id_option" value="{{$lookup->id}}">{{$lookup->name}}</option>
  @endforeach
</select>

<br>
<input type="submit" >

</form>

</div>
</div>
</div>
@include('_layout.footer')

