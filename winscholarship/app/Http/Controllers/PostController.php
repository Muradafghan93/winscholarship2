<?php

namespace App\Http\Controllers;

use App\post;
use App\lookups;
use Illuminate\Http\Request;


class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post=post::get();
        return response()->view('post\table',['post'=>$post]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lookups_loc=lookups::where('cat_id',1)->get();
        $lookups_level=lookups::where('cat_id',2)->get();
        $lookups_field=lookups::where('cat_id',3)->get();
        $lookups_req=lookups::where('cat_id',4)->get();
        return response()
        ->view('post\add',
        ['lookups_loc'=>$lookups_loc,'lookups_level'=>$lookups_level,
        'lookups_field'=>$lookups_field,'lookups_req'=>$lookups_req]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // return $request;
        post::create($request->only(['title','body','fb_link','end_date','location_id','field_of_study_id','study_level_id','english_requirment_id','abroad']));
        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(post $post)
    {
        $post=post::with(['location','english','field','level'])->where('id',$post->id)->get()[0];
      //  return $post;
        return response()->view('post\show',['post'=>$post]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(post $post)
    {
        //$post1=post::find($post);
        //return $post;
        return response()->view('post\edit',['post'=>$post]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, post $post)
    {
        $post->update($request->all());
        return $this->index();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(post $post)
    {
        $post->delete();
        return $this->index();
    }

    public function display()
    {
        $posts=post::paginate(8);
        return response()->view('post\postdisplay',['posts'=>$posts]);
    }
}
