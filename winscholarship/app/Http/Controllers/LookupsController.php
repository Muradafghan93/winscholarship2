<?php

namespace App\Http\Controllers;

use App\Lookups;
use Illuminate\Http\Request;

class LookupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $lookups=lookups::get();
        return response()->view('lookups\table',['post'=>$lookups]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lookups_cat=lookups::where('cat_id',null)->get();
        $lookups=lookups::get();


        return response()->view('lookups\add',['lookups_cat'=>$lookups_cat,'lookups'=>$lookups]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        lookups::create($request->only(['name','parent_id','cat_id']));
        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Lookups  $lookups
     * @return \Illuminate\Http\Response
     */
    public function show(Lookups $lookups)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Lookups  $lookups
     * @return \Illuminate\Http\Response
     */
    public function edit(Lookups $lookups)
    {
        return response()->view('lookups\edit',['lookup'=>$lookups]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lookups  $lookups
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lookups $lookups)
    {
        $lookups->update($request->all());
        return $this->index();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Lookups  $lookups
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lookups $lookups)
    {
        $lookups->delete();
        return $this->index();
    }
}
