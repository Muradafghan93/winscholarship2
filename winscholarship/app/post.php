<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class post extends Model
{
    protected $fillable=['body','title','fb_link','end_date','location_id','field_of_study_id','study_level_id','english_requirment_id','abroad'];
    

    public function location()
    {								
        return $this->belongsTo('App\Lookups', 'location_id');
    }

    public function field()
    {								
        return $this->belongsTo('App\Lookups', 'field_of_study_id');
    }

    public function level()
    {								
        return $this->belongsTo('App\Lookups', 'study_level_id');
    }

    public function english()
    {								
        return $this->belongsTo('App\Lookups', 'english_requirment_id');
    }
    

    
}
