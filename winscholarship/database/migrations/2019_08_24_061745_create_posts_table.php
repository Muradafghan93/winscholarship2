<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id')->unique();
            $table->string('title');
            $table->text('body');
            $table->string('photo_path')->default('null');
            $table->string('fb_link');
            $table->integer('no_of_read')->default(0);
            $table->integer('rating')->default(1);
            $table->unsignedInteger('location_id')->nullable();
            $table->unsignedInteger('field_of_study_id')->nullable();
            $table->unsignedInteger('study_level_id')->nullable();
            $table->unsignedInteger('english_requirment_id')->nullable();
            $table->integer('abroad')->nullable();
            $table->date('end_date');
            $table->timestamps();

        //foreign keys

            $table->foreign('location_id')->references('id')->on('lookups');
            $table->foreign('field_of_study_id')->references('id')->on('lookups');
            $table->foreign('study_level_id')->references('id')->on('lookups');
            $table->foreign('english_requirment_id')->references('id')->on('lookups');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
